import numpy as np
from collections import defaultdict, Counter
import os, logging

SAVE_PATH = os.path.dirname(os.path.abspath(__file__)) + "/src/vocab/"

def sampling_table(token_counter, mapping, eps=5e-5):
    """Gibt ein Tupel mit jeweils einer Liste als Eintrag zurück.
    Der erste Eintrag enthält eine Liste mit WSK das jeweilige
    Token (Index über mapping bestimmt) zu behalten beim Trainig.
    Der zweite enthält eine Liste mit WSK beim neg. Sampling negativ
    gesamplet zu werden."""

    # Terme in der Formel für Subsampling (siehe Dokumentation)
    items = token_counter.items()
    total_occurences = sum(occ for _, occ in items)
    total_occurences_pow = sum(occ ** (3/4) for _, occ in items)

    p_keeps, p_negs = [], []

    # aufsteigend nach Index sortierte Tokens
    sort = sorted(mapping.items(), key=lambda x: x[1])
    for token, _ in sort:
        
        # relative Häufigkeit
        rel_freq = token_counter[token] / total_occurences
        
        # erste Formel (siehe Dokumentation)
        p_keep = min((1 + np.sqrt(rel_freq / eps)) * (eps / rel_freq), 1)

        # zweite Formel (siehe Dokumentation)
        p_neg = int(token_counter[token]) ** (3/4) / total_occurences_pow

        # Füge zur Tabelle hinzu
        p_keeps.append(p_keep)
        p_negs.append(p_neg)

    return p_keeps, p_negs

def extract_vocab(path, vocab_list=None, vocab_size=0, lower=True,
                  filter_most_frequent=20, min_token_size=2):
    """Erhält einen Pfad zu einem Text, aus welchem ein Vokabular
    extrahiert wird, d.h. Häufigkeit/Vorkommen jedes Wortes werden 
    ermittelt und ggf. Einschränkung auf angegebene Vokabelliste,
    -größe.
    
    :param str path: Pfad zum Korpus
    :param list vocab_list: Liste von Strings, die ein Vokabular
                            repräsentieren. Die Ausgabe wird auf
                            diese Wörter beschränkt.
    :param int vocab_size: Zahl, die die maximale Vokabulargröße
                           angibt. Das Vokabular wird - wenn kein
                           Vokabular angegeben ist - auf die häu-
                           figsten vocab_size Wörter beschränkt.
    :param bool lower: Falls True, wird jedes Wort zunächst
                       kleingeschrieben, bevor irgendwelche Ope-
                       rationen durchgeführt werden. (default=
                       True).
    :param int filter_most_frequent: Bei der Bestimmung des Vo-
                                     kabulars werden die häufig-
                                     sten filter_most_frequent
                                     Tokens ignoriert.
    :param int min_token_size: Ignoriert alle Token, die weniger
                               Zeichen als min_token_size ent-
                               halten."""

    max_line = 0
    vocab_path = SAVE_PATH + os.path.basename(path) + ".voc"
    MAX_BATCH_SIZE = 5000000000

    try:    # Überprüfe ob ein Vokabular zum Korpus existiert
        voc = load_vocab_from_file(vocab_path)
        return voc

    # FileNotFoundError und sämtliche Formatierungsfehler
    # ließen das Programm sonst abstürzen
    except Exception:   
        print("Das Vokabular muss zunächst erstellt werden.")

        count = Counter()

        with open(path, "r") as f:      # Öffne Korpus im Kontextmanager und iteriere
            while True:                 # über die Satzblöcke

                # Einlesen der Sätze
                sents = f.readlines(MAX_BATCH_SIZE)

                if not len(sents):      # Abbruchbedingung, falls Dateiende
                    break

                # Addiere zum Satzzähler
                max_line += len(sents)
                # aufsplitten in Tokens
                sents = map(str.split, sents)

                # Zähle Tokens ggf. nur kleingeschrieben
                low = lambda x: x.lower() if lower else x
                count = count + Counter(low(token) for sent in sents 
                                               for token in sent
                                               if len(token) >= min_token_size)

        # Sollen frequente Tokens gefiltert werden?
        if filter_most_frequent:
            count = Counter(dict(count.most_common()[filter_most_frequent:]))

        # Soll ein bestimmmtes Vokabular genommen werden?
        if vocab_list:
            if lower:
                vocab_list = [voc.lower() for voc in vocab_list]

            # behalte nur Elemente aus dem Vokabular
            count = Counter(dict([token for token in count.items() if token[0] in vocab_list]))

        # Soll das Vokabular auf die häufigsten vocab_size
        # Tokens beschränkt werden?
        if vocab_size > 0:
            frequent = count.most_common(vocab_size)
            count = Counter(dict(frequent))

        token_counter = defaultdict(int, count.items())
        mapping = defaultdict(int, {tok: i+1 for i, tok in enumerate(token_counter.keys())})
        reverse_m = defaultdict(str, zip(mapping.values(), mapping.keys()))

        # Speichere Vokabular für nächsten Aufruf
        save_vocab_to_file(vocab_path, token_counter, mapping, max_line)

        print("Vokabular erstellt.")
        return token_counter, mapping, reverse_m, max_line


def save_vocab_to_file(path, token_counter, mapping, max_line):
    """Speichert das Vokabular als .voc-Datei, damit es beim
    nächsten Start nicht neu extrahiert werden muss.
    
    :param str path: Pfad zum Speicherort
    :param collections.defaultdict token_counter: Dict mit Häu-
                                                  figkeiten &
                                                  Vokabular
                                                  (Value & Keys)
    :param collections.defaultdict mapping: Dict mit wie oben, nur
                                            mit Indexzuordnungen
                                            als Values.
    :param int max_line: Korpusgröße in Zeilen."""

    # Speichersequenz
    with open(path, "w") as f:
        f.writelines(["MAXLINE={}\n".format(max_line)])
        for token, count, mapID in zip(token_counter.keys(), token_counter.values(), mapping.values()):
            f.write("{};{};{}\n".format(token, count, mapID))

def load_vocab_from_file(path):
    """Lädt eine .voc-Datei für ein Vokabular.
    
    :param str path: Pfad zur .voc-Datei.
    
    :return: Token Zähler, Abbildungswörterbuch,
             Umkehrabbildungswörterbuch, Anzahl Zeilen
    :rtype: tuple"""

    token_counter = defaultdict()
    mapping = defaultdict()

    # Ladesequenz
    with open(path, "r") as f:
        max_line = f.readline().split("=")[1]
        for line in f:
            t, c, mID = tuple(line.split(";"))
            
            token_counter[t] = int(c)
            mapping[t] = int(mID)
    reverse_m = defaultdict(str, zip(mapping.values(), mapping.keys()))

    return token_counter, mapping, reverse_m, max_line

def create_translation_dict(in_path_src, in_path_dest, out_path):
    """Erwartet zwei Textdateien, welche pro Zeile ein Wort
    enthalten. Die jeweiligen Zeilen korrespondieren und das
    Wort in Zeile n in Datei in_path_src wird zu dem Wort in
    Zeile n in in_path_dest übersetzt. Damit wird eine JSON-
    Datei, das Übersetzungswörterbuch,im Verzeichnis out_path
    erzeugt.
    
    :param str in_path_src: Pfad zu einer Textdatei mit einem
                            Wort pro Zeile (Quellsprache).
    :param str in_path_dest: Pfad zu einer Textdatei mit einem
                             Wort pro Zeile (Zielsprache).
    :param str out_path: Pfad an dem das Übersetzungswörterbuch
                         als JSON-Datei gespeichert werden soll."""

    import json

    voc_src, voc_dest = [], []

    # Lese Quellsprache
    with open(in_path_src, "r") as srcf:
        for line in srcf:
            voc_src.append(line.replace("\n", ""))
    # Lese Zielsprache
    with open(in_path_dest, "r") as destf:
        for line in destf:
            voc_dest.append(line.replace("\n", ""))
    # Schreibe JSON-Datei
    with open(out_path, "w") as transf:
        json.dump(dict(zip(voc_src, voc_dest)), transf)