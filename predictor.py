import numpy as np
from scipy.stats import t
import re, os
from src.treetagger.treetagger import TreeTagger

mlvs = None

def set_multilang_vectorspace(multilang_vs):
    global mlvs

    mlvs = multilang_vs

def predict_metaphor(sent):
    """Entscheidet ob der vorliegende String von Tokens
    eine Metapher ist oder nicht.
    
    :param str sent: Satz der auf Metaphern untersucht
                     werden soll
    
    :return: 'y' oder 'n'
    :rtype: str"""

    crucial, significant = _crucial_combination(sent.split())
    if significant:
        print("Als Metapher gewertet, mit ausschlaggebendem Wortpaar", crucial)
        return "y"
    return "n"

def _crucial_combination(sent):
    """Kernfunktion um zu überprüfen, welche Wortkombination 
    am stärksten zueinander im Kontrast steht und am wenigsten
    zu den restlichen Worten passt.
    
    :param list sent: Liste von Tokens
    
    :return: die stärkste Wortkombination + Signifikanz dieser
    :rtype: tuple"""

    # Lemmatisieren via treetagger
    UNKNOWN_LEMMA = "<unknown>"
    CARD_LEMMA = "@card@"
    tt_path = os.path.dirname(os.path.realpath(__file__)) + "/src/treetagger/postag"
    tt = TreeTagger(path_to_treetagger=tt_path, language='german')

    # rausfiltern von Tokens die nicht gebraucht werden,
    # zusätzlich wird alles kleingeschrieben
    use_token = lambda x: x[2] in [UNKNOWN_LEMMA, CARD_LEMMA] or x[1] in ['NE', 'CARD', 'XY', 'FM', "$."]
    get_tok = lambda x: x[0].lower() if use_token(x) else x[2].lower()      # entscheide hier ob lemma oder token selbst genommen wird

    # weitere Ausschlusskriterien, unerlaubte Zeichen und OOV
    is_latin = lambda x: all(bool(re.search('[a-zA-ZäöüÄÖÜß]', char)) for char in x)
    is_usable = lambda x: is_latin(x) and mlvs.mc.in_vocab(get_tok(x), "de")

    # Für das richtige Format in der Liste
    get_tuple = lambda x, y: (get_tok(x), get_tok(y))

    # Alle möglichen Wortkombinationen bilden
    cart_prod = [get_tuple(token1, token2) for i, token1 in enumerate(tt.tag(sent))
                                           for token2 in tt.tag(sent)[i:]
                                           if not token1 == token2 and
                                           is_usable(token1) and is_usable(token2)]      # beide nur Zeichen der dt. Sprache
    sims = []

    # Jede Sprache wird berücksichtigt.
    for lang in mlvs.mc.get_languages():

        # Über jede Tokenkombination wird iteriert.
        for token1, token2 in cart_prod:

            # gib jedem Paar einen Ähnlichkeitswert
            if lang == "de":
                sim = mlvs.similarity((token1, lang), (token2, lang), metric="dot")
                sims.append((token1, token2, lang, sim))
            else:
                # Übersetze Tupel zunächst
                trans1 = mlvs.mc.translate(token1, "de", lang)
                trans2 = mlvs.mc.translate(token2, "de", lang)

                # Alle möglichen Übersetzungen werden auf
                # Ähnlichkeit überprüft
                for t1 in trans1:
                    for t2 in trans2:

                        # Ähnlichkeit der Übersetzungen
                        sim = mlvs.similarity((t1, lang), (t2, lang), metric="norm")
                        sims.append((token1, token2, lang, sim))      # token1,token2 weiterhin in de

    # final_sim enthält alle rückübersetzen tokens
    # mit dem Ähnlichkeitswert der Sprache aus der
    # es zurückübersetzt wurde
    final_sim = dict()
    for token1, token2, _, simil in sims:
        final_sim.setdefault((token1, token2), []).append(simil)

    # initialisiere Rückgabewert
    crucial = None
    significant = None

    # Falls genug Ergebnisse gefunden wurden um Aussage zu treffen
    if len(final_sim):
        
        # hänge die Datenpunkte für die Signifikanzprüfung an
        x, y = [], []
        for _, simlist in final_sim.items():
            x.append(simlist[1:2])
            y.append(simlist[2:3])

        # setze das ausschlaggebene Paar (maximale Ähnlichkeit)
        crucial = max(final_sim.items(), key=lambda x: x[1][0])[0]

        # Prüfe auf Signifikanz
        index_cruc = [i for i, (combi, _) in enumerate(final_sim.items()) if combi == crucial][0]
        signif_l1 = _significant(x, index_cruc)
        signif_l2 = _significant(x, index_cruc)

        # Ist einer signifikant, so entscheiden wir für eine Metapher
        if signif_l1 is None and signif_l2 is None:
            # nicht genügend Information, daher zufällig
            significant = np.random.choice([True, False], p=[0.5, 0.5])
        else:
            significant = signif_l1 if not signif_l1 is None else signif_l2

    return crucial, significant


def _significant(x, index, alpha=0.1):
    """Testet ob ein Token in seinem Satz signifikant ist.
    (Siehe mehr dazu in der Dokumentation)
    
    :param list x: Datenpunkte mit Ähnlichkeitswerten
    :param int index: Index des zu testenden Tokens in x
    :param int alpha: Signifikanzniveau zu dem getestet wird.
    
    :return: Signifikanz des Tokens
    :rtype: bool."""

    if not len(x) > 2:
        return      # None, da keine Aussagekraft

    # Parameter bestimmen, zwei Stichproben t-Test
    n, m = len(x) - 1, 1

    # Mittelwert, Varianz, und anderer Varianzterm
    mu = np.mean([val[0] for i, val in enumerate(x) if not i == index])
    sigma2 = np.var([val[0] for i, val in enumerate(x) if not i == index])
    S2 = (n-1) * sigma2 / (n + m -2)

    print(mu, sigma2, S2)

    # Teststatistik + kritischer Wert
    T, critical_value = [0.0], t.ppf(1-alpha/2, n+m-2)
    if S2 * (1/n + 1/m):
        T = (mu - x[index]) / np.sqrt(S2 * (1/n + 1/m))

    return (np.abs(T) > critical_value)[0]


def find_synonyms(token, lang_code="de", metric="combined"):
    """Akzeptiert ein Token im Format ('token', 'de'), etc...
    Gibt anschließend alle Synonyme des Tokens der angegeben 
    Sprache zurück.
    
    :param tuple token: Ein Token zu dem Synonyme bestimmt
                        werden sollen.
    :param str lang_code: Sprachcode der Sprache, in der
                          die Synonymsuche stattfinden soll.
    :param str metric: Metrik, welche benutzt wird. Derzeit
                       nur 'combined' möglich.
    
    :return: Liste von Synonymen (str)
    :rtype: list"""

    close_intersections = []

    # Benutze die in der Dokumentation beschriebene Metrik
    if metric == "combined":
        for lang in mlvs.mc.get_languages():
            translations = []

            if not lang == lang_code:
                trans = mlvs.mc.translate(token, lang_code, lang)
                translations = [(tr, lang) for tr in trans]
            for tok in ([token] + translations):

                # Vektor der Zielsprache
                vec = mlvs.embeds.token_to_vector[tok]

                close1 = [tok for _, tok in mlvs.embeds.get_closest(vec, num=20, metric="dot", maximize=True)]
                close2 = [tok for _, tok in mlvs.embeds.get_closest(vec, num=20, metric="norm")]

                # Die Mengenoperationen dauern leider sehr, sehr lange.
                # close1 = set([tok for _, tok in mlvs.embeds.get_closest(vec, num=20, metric="dot", maximize=True)])
                # close2 = set([tok for _, tok in mlvs.embeds.get_closest(vec, num=20, metric="norm")])       # hjier gleich mal dot
                close_intersections.append([a for a in close1 if a in close2])

        finals = []
        for close in close_intersections:
            # Übersetze jedes Token einer anderen Sprache zurück
            for (tok, lang) in close:

                # Das gleiche Wort ist kein Synonym
                if not tok == token:

                    # nur andere Sprachen rückübersetzen
                    if not lang == lang_code:
                        finals += [trans for trans in mlvs.mc.translate(tok, lang, lang_code)
                                if not trans == token]

                    else:   # ansonsten wie gehabt, behalten
                        finals.append(tok)

    if not len(finals):
        return baseline_syn(token, lang_code=lang_code)

    return list(set(finals))[:10]


def synonymous(token1, token2, lang_code="de"):
    """Überprüft, ob die angegeben Tokens in der
    angegebenen Sprache synonym sind.
    
    :param str token1: erstes zu testendes Token
    :param str token2: zweites zu testendes Token
    :param str lang_code: Sprachchode der Sprache in welcher
                          die Synonymfindung stattfinden soll
                          
    :return: String mit Wert ’y’, falls die angegebenen
             Tokens synonym sind bzw. 'n' falls nicht.
    :rtype: str"""

    fsyn = lambda x: find_synonyms((x, lang_code))
    symm_check = lambda x,y: x in fsyn(y) or y in fsyn(x)
    # print(fsyn(token1), fsyn(token2))
    return "y" if symm_check(token1, token2) else "n"

def baseline_syn(token, lang_code="de"):
    
    vec = mlvs.embeds.token_to_vector[token]
    return [token for _, (token, lang) in mlvs.embeds.get_closest(vec, num=20, metric="norm", maximize=False) if lang == lang_code][:10]

