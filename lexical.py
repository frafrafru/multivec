from collections import Counter
import logging
import multiprocessing as mp
import utils

NO_TRANSLATION_DICT_WARNING = """Es wurde kein Übersetzungswörterbuch hinzugefügt,
                              da kein Pfad angegeben wurde. Für den reibungslosen
                              Ablauf des Programmes ist dieses aber empfohlen und
                              kann hinzugefügt werden, mittels
                              lexical.MultiCorpus.add_translation_dict()."""
NOT_ENOUGH_LANG_TRANSLATION_DICT_WARNING = """Es existieren nicht genügend Sprachen,
                                           um Übersetzungswörterbücher hinzuzufügen."""
LANG_EXIST_WARNING = "Sprache bereits vorhanden und wird nicht hinzugefügt."
TRANSLATION_DICT_ALREADY_EXIST_WARNING = "Sprachkombination bereits vorhanden, wird nicht hinzugefügt."
TRANSLATION_DICT_NOT_FOUND_ERROR = "Übersetzungswörterbuch nicht gefunden! ({})"

class MultiCorpus:

    import json

    def __init__(self):
        """Initialisiert einen Multicorpus, welcher mehrere Korpora
        verschiedener Sprachen organisiert."""

        self.paths = []                 # Pfade
        self.lang_codes = dict()        # Sprachcodes
        self.vocabs = []                # Tripel mit (token_counter, mapping, rev_m)
        self.translation_dicts = []     # Übersetzungswörterbücher
        self.joined_mapping = None      # Mapping auf globales Vokabular
        self.first_lang = None          # erste Sprache

    def add_corpora(self, paths, lang_codes, vocab_lists, vocab_sizes,
                    translation_dict_paths, lower=True, max_worker=10,
                    filter_frequent=20, min_token_size=2):
        """Fügt ein oder mehrere Korpora hinzu.

        :param list paths: Liste, (muss dieselbe Größe wie lang_codes,
                           vocab_lists und vocab_sizes haben) die Pfade
                           zu den Textdateien als String enthält.
        :param list lang_codes: Liste, die Sprachcodes enthält, die zu
                                den angegebenen Pfaden korrespondieren.
                                Die Elemente sind im Stringformat.
        :param list vocab_lists: Liste, die entweder None oder oder eine
                                 Liste von Wörtern (Strings) als Ele-
                                 mente enthält. Falls None: alle Wörter
                                 werden ins Vokabular aufgenommen. Bei
                                 Listenübergabe nur die, die in dieser
                                 Liste auftauchen.
        :param list vocab_sizes: Liste, die entweder None oder einen
                                 Integer n als Element enthält. Diese
                                 optionale Zahl ermöglicht das Vokabular
                                 auf die häufigsten n Wörter zu redu-
                                 zieren.
        :param list translation_dict_paths: Liste, die Strings als Ele-
                                            mente enthält mit Pfaden zu
                                            Übersetz ungswörterbüchern
                                            im JSON-Datei-format. Diese
                                            muss genau ein Element we-
                                            niger enthalten, als jede
                                            der oben genannten Listen.
                                            (Für jede Fremdsprache ein
                                            Wörterbuch)
        :param bool lower: Falls True, wird jedes Wort zunächst kleinge-
                           schrieben, bevor irgendwelche Operationen
                           durchgeführt werden. (standardmäßig: True)
        :param int max_worker: Wird mehr als ein Korpus hinzugefügt, so
                               wird das Vokabular parallelisiert ex-
                               trahiert. Diese Zahl gibt die maximale An-
                               zahl an Prozessoren an, die in diesem
                               Schritt alluziert werden. (standardmäßig:
                               10)
        :param int filter_frequent: Filtert die filter_frequent
                                         häufigsten Tokens aus dem Vokabular.
        :param min_token_size: Mindestgröße für Tokens, die ins Vokabular
                               aufgenommen werden."""

        # Setze die erste Sprache, falls noch nicht vorhanden
        if not self.first_lang:
            self.first_lang = lang_codes[0]

        # Im Schleifendurchlauf werden möglicherweise Elemente
        # aus der Schleife gelöscht, daher .copy()
        langs = lang_codes.copy()
        for i, lang in enumerate(langs):

            # Nehme die Sprachcodes ins Sprachcodewörterbuch
            # auf, falls nicht schon vorhanden.
            if not lang in self.lang_codes:
                self.lang_codes[lang] = len(self.lang_codes)
            else:
                # Lösche aus Parameterlisten; wird nicht hinzugefügt.
                logging.warning(LANG_EXIST_WARNING)
                map(list.pop, paths, lang_codes, vocab_lists, vocab_sizes, [i] * 4)

        # Übersetzungswörterbücher nur bei Mehrsprachigkeit benötigt
        if len(self.lang_codes) > 1:
            for k, (lang_code, td_path) in enumerate(zip(lang_codes,
                                                         translation_dict_paths)):

                # Überprüfe ob ein Übersetzungswörterbuch angegeben wurde
                # Falls ja, wählen wir dieses als Grundlage für das Vokabular
                if td_path:
                    vocab_lists[k] = self.add_translation_dict(self.first_lang,
                                                               lang_code,
                                                               td_path,
                                                               lower=lower)
                else:
                    logging.warning(NO_TRANSLATION_DICT_WARNING)
        else:
            logging.warning(NOT_ENOUGH_LANG_TRANSLATION_DICT_WARNING)

        # Hier: Multiprozessoraufruf, nur max. so viele 
        # Prozesse, wie es Korpora gibt.
        with mp.Pool(min(max_worker, len(paths))) as pool:

            # Parameter im Format [(arg1, arg2, ...), (arg1, arg2, ...), ...]
            arguments = [args for args in zip(paths, vocab_lists,
                                              vocab_sizes, [lower] * len(paths),
                                              [filter_frequent] * len(paths),
                                              [min_token_size] * len(paths))]
            result = pool.starmap(utils.extract_vocab, arguments)

            # Nehme jedes Ergebnis der Prozessoraufrufe ins Vokabular auf.
            for (token_counter, mapping, rev_mapping, max_line), lang, path in zip(result, lang_codes, paths):
                self.vocabs.append((token_counter, mapping, rev_mapping))
                self.paths.append((path, max_line))
 
        # globales Mapping verändert sich, bei neuer Sprache (RESET)
        # wird zu gegebener Zeit (d.h. wenn benötigt) neu initialisiert
        self.joined_mapping = None
        
    def add_corpus(self, path, lang_code, vocab_list=None, vocab_size=None,
                   translation_dict_path=None, lower=True, filter_frequent=20,
                   min_token_size=2):
        """Fügt ein Korpus hinzu.

        :param str path: Pfad zur Textdatei, die dieses Korpus beschreibt.
        :param str lang_code: Sprachcode des angegebenen Korpusses, z.B.
                              'de', 'en', etc.
        :param list vocab_list: Falls nicht None, so wird das Korpus auf
                                dieses Vokabular beschränkt.
        :param int vocab_size: Falls nicht None, wird das Korpus auf die
                               vocab_size häufigsten Wörter beschränkt.
        :param str translation_dict_path: optionaler Pfad zu einem Über-
                                          setzungswörterbuch im JSON-Format.
        :param bool lower: Falls True, wird jedes Wort zunächst kleingeschrieben, be-
                           vor irgendwelche Operationen durchgeführt werden. (default=
                           True)
        :param int filter_frequent: Filtert die filter_frequent
                                         häufigsten Tokens aus dem Vokabular.
        :param min_token_size: Mindestgröße für Tokens, die ins Vokabular
                               aufgenommen werden."""

        # Setze die erste Sprache, falls noch nicht vorhanden
        if not self.first_lang:
            self.first_lang = lang_code

        # Nehme Sprache ins Wörterbuch auf (s.o.)
        self.lang_codes[lang_code] = len(self.lang_codes)
        
        # Nur bei Mehrsprachigkeit wird ein Wörterbuch hinzugefügt.
        if len(self.lang_codes) > 1 and translation_dict_path:
            vocab_list = self.add_translation_dict(self.first_lang, lang_code, translation_dict_path, lower=lower)

        # keine Mehrsprachigkeit,
        elif translation_dict_path:
            logging.warning(NOT_ENOUGH_LANG_TRANSLATION_DICT_WARNING)

        # kein Pfad angegeben.
        elif len(self.lang_codes) > 1:
            logging.warning(NO_TRANSLATION_DICT_WARNING)

        # Aufbereitung des Vokabulars
        items = utils.extract_vocab(path, vocab_list, vocab_size, lower,
                                    filter_frequent, min_token_size)
        token_counter, mapping, rev_mapping, max_line = items

        # Aufnahme ins Vokabular: Korpuspfad + Vokabularinfos
        self.vocabs.append((token_counter, mapping, rev_mapping))
        self.paths.append((path, max_line))

        # globales Mapping verändert sich bei neuer Sprache (RESET)
        self.joined_mapping = None

    def get_token_counter(self, lang_code=None):
        """Gibt das gezählte Vokabular der angegebenen
        Sprache als collections.defaultdict zurück.
        
        :param str lang_code: Sprachcode der Sprache,
                              dessen gezähltes Vokabular
                              zurückgegeben werden soll
                              (z.B.: 'de', 'en', etc...).
                              Falls None wird der Token-
                              zähler für das gesamte mul-
                              tilinguale Vokabular zurück-
                              gegeben.
                              
        :return: Gezähltes Vokabular als Wörterbuch.
        :rtype: collections.defaultdict"""

        if lang_code is None:
            c = Counter()
            for lang, index in self.lang_codes.items():
                c += Counter({(token, lang): occ for token, occ in self.vocabs[index][0].items()})
            return c
        try:
            return self.vocabs[self.lang_codes[lang_code]][0]
        except KeyError:
            # Key lang_code nicht gefunden, d.h. Sprache nicht bekannt.
            logging.error("Für diese Sprache existiert kein Vokabular ({})".format(lang_code))

    def get_iterator(self, lang, start_line=1):
        """Erzeugt einen Iterator, der dem Python-Iterator Protokoll folgt
        und gibt ihn zurück.
        
        :param str lang: Sprachcode, der dem zu erzeugenden Korpus
                         entspricht.
        :param int start_line: optional: Zeile, ab der begonnen werden soll.

        :return: lexical.CorpusIterator Instanz
        :rtype: iterator, None falls eine ungültige Sprache angegeben wurde.
        """

        try:
            path, max_line = self.paths[self.lang_codes[lang]]
            return CorpusIterator(path, max_line, start_line=start_line)

        # Tritt auf, wenn Sprachcode nicht gefunden wurde.
        except KeyError:
            logging.error("Für diese Sprache ({}) existiert kein Korpus!".format(lang))

    def get_vocab_list(self, lang_code=None):
        """Gibt das Vokabular der angegebenen Sprache als Liste zurück.
        
        :param str lang_code: Sprachcode der Sprache des gewünschten
                              Vokabulars, z.B. 'de', 'ru', ...
                              Ist dieser Parameter None, so wird das
                              gesamte multilinguale Vokabular zurück-
                              gegeben.

        :return: Liste mit angegebenen Vokabular als Strings, falls
                 lang_code nicht None, sonst Liste im Format:
                 [('Wort1', 'de'), ..., ('Wort10051', 'ru'), ...]
        :rtype: list"""

        # keine Sprache angegeben -> gesamtes Vokabular
        if not lang_code:
            all_vocab = []
            for language, lang_id in self.lang_codes.items():
                voc_list = [token for token, _ in self.vocabs[lang_id][0].items()]
                all_vocab += [(token, language) for token in voc_list]

        try:
            # Vokabular aus dem token_counter, der in der Liste vocabs als erster Eintrag ist
            return list(self.vocabs[self.lang_codes[lang_code]][0].keys())
        except KeyError:
            logging.error("Für diese Sprache existiert kein Vokabular ({})".format(lang_code))

    def vocab_size(self, lang=None):
        """Gibt die Vokabulargröße der angegeben Sprache zurück.
        Falls nichts angegeben wurde, so wird die Größe des
        globalen Vokabulars zurückgegeben.
        
        :param str lang: Sprachcode, der Sprache, z.B. 'de', etc...
        
        :return: Vokabulargröße
        :rtype: int."""

        return len(self.get_mapping(lang))
        
    def in_vocab(self, token, lang_code):
        """Überprüft ob das angegebene Token in dem Vokabular
        der Sprache lang enthalten ist.
        
        :param str token: Token, welches überprüft wird.        
        :param str lang_code: Sprache des zu überprüfenden Vokabulars.

        :return: Wahrheitswert, ob token im Vokabular der Sprache
        :rtype: bool."""

        if token == '':     # Häufiger Fall, daher Sonderprüfung
            return False
        return token in self.vocabs[self.lang_codes[lang_code]][0]

    def token_to_id(self, token, lang, glob=True):
        """Gibt die zu einem Token gehörende ID zurück.
        Die ist innerhalb einer Sprache und global (d.h.
        glob=True) eindeutig. Falls das Token nicht im
        Vokabular ist, so wird 0 zurückgegeben.
        
        :param str token: Token, dessen ID zurückgegeben werden soll.
        :param str lang: Sprachcode der Sprache aus der token stammt.
        :param bool glob: Falls True, bezieht sich die ID auf
                          das globale, also multilinguale Wörterbuch.
        :return: ID des Tokens im Vokabular.
        :rtype: tuple der Form ('tok1', 'de'), falls glob=True,
                sonst int."""

        try:
            if glob: 
                # global dict hat tupel (TOKEN, LANG)
                # rufe interne Methode hierfür auf.
                return self._joined_mapping()[(token, lang)]
            return self.get_mapping(lang)[token]

        except KeyError:    # nicht im Vokabular
            # logging.warning("Token {} ({}) ist nicht im Vokabular".format(token, lang))
            return 0

    def get_mapping(self, lang=None):
        """Gibt die Bijektion (Wörterbuch) zwischen Vokabular
        und [1, ..., N], mit Vokabulargröße N, zurück.
        
        :param str lang: Falls nicht None, wird das sprach-
                         spezifische Wörterbuch zurückgegeben.
                         
        :return: Wörterbuch der Zuordnung zwischen Vokabular
                 und [1, ..., N]
        :rtype: list"""

        # Standardmäßig, wenn nichts angegeben: globales Mapping
        if not lang:
            return self._joined_mapping()
        return self.vocabs[self.lang_codes[lang]][1]

    def get_rev_mapping(self, lang=None):
        """Gibt die Bijektion (Wörterbuch) zwischen Vokabular
        und [1, ..., N], mit Vokabulargröße N, zurück. (Umkehrung
        zu lexical.get_mapping())
        
        :param str lang: Falls nicht None, wird das sprach-
                         spezifische Wörterbuch zurückgegeben.
                         
        :return: Wörterbuch der Zuordnung zwischen Vokabular
                 und [1, ..., N]
        :rtype: list"""

        # Standardmäßig, wenn nichts angegeben: globales Mapping
        if not lang:
            jm = self._joined_mapping()
            return dict(zip(jm.values(), jm.keys()))
        return self.vocabs[self.lang_codes[lang]][2]


    def get_languages(self):
        """Gib alle vorhandenen Sprachchodes in einer
        Liste zurück.
        
        :return: Liste mit vorhandenen Sprachchodes.
        :rtype: list."""

        return list(self.lang_codes.keys())

    def translate(self, word, src, dest):
        """Gibt alle bekannten Übersetzungen des Wortes word
        aus der Ausgangssprache src in die Zielsprache dest
        als Liste zurück.
        
        :param str word: Wort, welches übersetzt werden soll.
        :param str src_lang: Sprachcode der Ausgangssprache,
                             z.B. 'de', 'en', etc...
        :param str dest_lang: Sprachcode der Zielsprache.
        
        :return: Liste mit Übersetzungen als Strings
        :rtype: list"""
    
        try:
            # Suche Üb.-Wörterbuch mit dieser Sprachkombination
            tdict = [translation_dict for translation_dict, src_lang, dest_lang in self.translation_dicts
                     if (src, dest) == (src_lang, dest_lang)][0]
            return [part for part in tdict[word].split()]

        # Tritt auf bei 'tdict[word]', falls 'word' nicht gefunden
        except KeyError:
            logging.info("Keine Übersetzung gefunden für {} ({})".format(word, dest))
        
        # Tritt auf bei [translation_dict for ...][0], wenn diese Liste
        # leer ist, d.h. kein Wörterbuch vorhanden.
        except IndexError:
            logging.info("Kein Wörterbuch gefunden für ({}, {})".format(src, dest))
        return []

    def add_translation_dict(self, src, dest, translation_dict_path,
                             return_vocab=True, add_reverse=True, lower=True):
        """Fügt ein Übersetzungswörterbuch hinzu, falls es nicht bereits
        eins für diese Sprachkombination gibt.

        :param dict translation_dict: Ein dict Objekt, welches als
                                      Keys Wörter der Quellsprache
                                      und als Values Wörter der
                                      Zielsprache besitzt
        :param str src_lang: Sprachcode der Ausgangssprache, z.B.:
                             'de', 'ru', etc.
        :param str dest_lang: Sprachcode der Zielsprache, z.B.:
                              'de', 'ru', etc.
        :param bool return_vocab: Falls True, wird das Vokabular als
                                  Liste zurückgegeben. (default:True)
        :param bool add_reverse: Falls True wird auch das Rücküber-
                                 setzungswörterbuch hinzugefügt.
        :param bool lower: Falls True werden die Übersetzungen
                           kleingeschrieben ins Wörterbuch hinzugefügt.

        :return: Vokabular der Zielsprache, falls return_vocab=True
                 ist, andernfalls None
        :rtype: list"""

        # Überprüfe, ob bereits vorhanden
        if (src, dest) in [(sl, dl) for _, sl, dl in self.translation_dicts]:
            logging.warning(TRANSLATION_DICT_ALREADY_EXIST_WARNING)

        else:
            try:
                # Falls Parameter add_reverse=True benötigt
                rev_trans_dict = None

                # Öffne JSON-Datei
                with open(translation_dict_path) as translation_dict:
                    t_dict = self.json.load(translation_dict)
                    if lower:
                        t_dict = {key.lower(): value.lower() for key, value in t_dict.items()}
                    self.translation_dicts.append((t_dict, src, dest))
    
                    if add_reverse:
                        rev_trans_dict = {}
                        for key, value in t_dict.items():
                            for value in value.split():
                                rev_trans_dict.setdefault(value, []).append(key)

                        if lower:
                            rev_trans_dict = {key.lower(): " ".join(value).lower() for key, value in rev_trans_dict.items()}
                        else:
                            rev_trans_dict = {key: " ".join(value) for key, value in rev_trans_dict.items()}
                        self.translation_dicts.append((rev_trans_dict, dest, src))
                
            # Beim Öffnen JSON-Datei nicht gefunden
            except FileNotFoundError:
                logging.error(TRANSLATION_DICT_NOT_FOUND_ERROR.format(translation_dict_path))
                return None

            # Falls angegeben, wird übersetztes Vok. zurückgegeben
            if return_vocab:
                return list(set([part for _, translation in t_dict.items() for part in translation.split()]))

    def _joined_mapping(self):
        """Interne Methode um das Mapping auf das globale Vokabular
        zu handlen und zurückzugeben.
        
        :return: Mapping (Wörterbuch) auf das globale Vokabular
        :rtype: dict."""

        if not self.joined_mapping:
            self._create_joined_mapping()
        return self.joined_mapping

    def _create_joined_mapping(self):
        """Interne Methode, um das Mapping auf das globale Vokabular
        zu erstellen."""

        temp_dict = dict()
        for (_, mapping, _), language in zip(self.vocabs, self.lang_codes.keys()):
            for token in mapping.keys():
                temp_dict[(token, language)] = len(temp_dict) + 1     # da 0 padding element ist
        self.joined_mapping = temp_dict


class CorpusIterator:

    def __init__(self, path, max_line, start_line=1):
        """Erzeugt einen  Iterator, der durch das über den
        Pfad angegebene Korpus iteriert.
        
        :param str path: Pfad zum Korpus.
        :param int max_line: Anzahl der Zeilen des Korpus
                             (wird für die Fortschritts-
                             anzeige benötigt)
        :param int start_line: Von der Zeile, ab der iteriert
                               werden soll."""

        # Öffne Datei und setze Parameter klassenintern
        self.f = open(path, "r")
        self.max_line = max_line
        self.current_line = mp.Value('i', 0)
        self.start_line = start_line

    def __iter__(self):
        return self

    def __next__(self):

        sents = []

        # Solange bis die Anfangszeile erreicht wurde
        while True:
            try:
                # Lese Sätze
                sents = self.f.readlines(500000)
                
                if len(sents):  # weitere Sätze gefunden

                    # Anfangszeile ist im jetztigen Block enthalten
                    if self.current_line.value + len(sents) >= self.start_line:
                        break

                    # Anfangszeile ist nicht im jetztigen Block
                    # enthalten, also weiter zählen
                    self.current_line.value += len(sents)

                else:           # Ende der Datei
                    self.f.close()
                    raise StopIteration

            # Für den Fall, dass Encodingprobleme o.ä. auftreten.
            except Exception as e:
                logging.error(e, self.f.name)

        # Differenz bis zur Anfangszeile
        diff = self.start_line - self.current_line.value
        # Alles ab der Anfangszeile
        sents = sents[diff:]
        self.current_line.value += len(sents)

        # Gib die Batch als Liste von Liste von Tokens zurück.
        return [[token for token in sent.split() if token] for sent in sents if sent]

    def progress(self, digits=3):
        """Gibt den Fortschritt der Iteration bis zu den
        angegebenen Nachkommastellen in Prozentangaben
        zurück.
        
        :param int digits: Anzahl der gerundeten Nachkommastellen.
        
        :return: Fortschritt der Iteration in Prozent
        :rtype: int"""

        return round(self.current_line.value / self.max_line * 100, digits)
