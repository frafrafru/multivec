import os, logging, re

import multiprocessing as mp
import numpy as np

import evaluation as evl
from multilang import MultiLangVectorspace
import predictor as p

possible_inp = "Mögliche Eingaben: \n\t  - 'haus,de'\n\t  - 'House,en + gArtEn,de'\n\t  - 'krankenhaus,de - right,en'\n\t  - etc...\n"

help_strings = dict()
help_strings['c'] = possible_inp + "\n\tLiefert dann die n (siehe 'n') Vektoren, welche der Summe der angegebenen am nächsten sind."
help_strings['s'] = possible_inp + "\n\tLiefert dann die n (siehe 'n') Vektoren, welche zu der Summe der angegebenen synonym sind."
help_strings['met'] = "Testet ob angegebener Satz eine Metapher ist. Die Eingabe erfolgt hier als normaler Satz."
help_strings['t'] = "Trainiert das Modell. Dabei wird gefragt, wie viele Iterationen. Die Anzahl der Epochen kann via 'e' gesetzt"
help_strings['m'] = "Setzt die Metrik. Die Wahl besteht aus: 'dot', 'norm', 'cos'. Anschließend wird gefragt, ob diese Metrik maximiert oder minimiert werden soll, wenn die Abstandsberechnung mit 'c' durchgeführt wird."
help_strings['n'] = "Setzt das n, d.h. die Anzahl der Iterationen, die pro Trainingsdurchlauf"
help_strings['e'] = "Setzt die Anzahl der Epochen, die beim Training (pro Batch) durchlaufen werden sollen."
help_strings['eval'] = "Evaluiert das System auf Synonympaare, -mengen und/oder Metaphern. Dialog bei Auswahl."
help_strings['h'] = "Zeigt die Befehlsübersicht an."


# globale Variablen
mlvs = None         # MultiLangVectorspace Objekt
metric = "norm"     # Verwendete Metrik
maximize = False    # Maxi- oder Minimierung bei der Abstandsberechnung
N = 25              # Anzahl Ausgaben (bei nächste Vektoren/Synonyme)
epochs = 12         # Anzahl Trainigsdurchläufe pro Batch

def start_init_sequence():
    """Initialisierung des Systems."""

    global mlvs

    msg = """Programm gestartet.\n----------------------\nDas Modell wird initialisiert"""
    print(msg)

    # Initialisiere Pfade
    src_path = os.path.dirname(os.path.realpath(__file__)) + "/src/"
    train_path = src_path + "training/{}_train.ing"     # Trainingsdateienpfad
    tr_dict_path = src_path + "dicts/{}-{}.tradict"     # Übersetzungswörterbücherpfad

    # Initialisiere Sprachen und deren Übersetzungswörterb.
    langs = ["de", "ru", "en"]
    paths = [train_path.format(lang) for lang in langs]
    translation_dicts = [tr_dict_path.format("de", "ru"),
                         tr_dict_path.format("de", "en")]

    # Initialisiere den multilingualen Vektorraum
    mlvs = MultiLangVectorspace(paths, langs, vocab_sizes=[10000, 0, 0],
                                translation_dict_paths=translation_dicts,
                                window_size=8, embed_dim=200)

    # Initialisiere den Predictor
    p.set_multilang_vectorspace(mlvs)
    evl.set_predictor(p)
    evl.set_vocab(mlvs.mc.get_vocab_list("de"))

start_init_sequence()

def _word_in_seq():
    """Startet Eingabesequenz, welche es erlaubt, ein
    oder mehrere Tokens anzugeben, welche dann als Liste
    von Tupeln bereitgestellt werden."""

    input_words = []
    input_word = input("Wort eingeben: ")
    input_word = input_word.replace("-", "+-").lower()

    for term in input_word.split("+"):

        if not len(term):
            continue

        if "," not in term:
            logging.warning("Falsches Eingabeformat. (Mögliche Eingabe: house, en + blau, de - семья, ru")
            return None
        else:
            token, lang = tuple(term.split(","))
            pos = (not token.strip()[0] == "-")
            input_words.append(((token.strip().replace("-", ""), lang.strip()), pos))

    return input_words

def _closest_seq():
    """Startet die Sequenz, um den/die nächsten Vektor(en)
    ausfindig zu machen."""

    tokens = _word_in_seq()
    positives, negatives = [], []
    if tokens:
        for token, pos in tokens:
            if pos:
                positives.append(token)
            else:
                negatives.append(token)

        print("\nVerwendete Metrik: {}".format(metric))
        sims = mlvs.most_similar(positive=positives, negative=negatives,
                                 num=N, metric=metric, maximize=maximize)
        for dist, (token, lang) in sims:
            print("{} \t\t ({}) \t: {}".format(token, lang, round(dist, 3)))

def _print_help():
    """Gibt die Hilfe in der Konsole aus."""

    global help_strings

    print("Hilfe")
    print("-"*100)

    for key, info in help_strings.items():
        print("  Taste {}:".format(key))
        print("\t", info, "\n")

def run():
    """Hauptfunktion, damit das Programm läuft"""

    global mlvs, N, metric, epochs, maximize
    
    _print_help()

    while True:
        inpt = input("... ")

        try:
            if inpt == "c":             # Die nächsten Vektoren
                _closest_seq()
                
            elif inpt == "s":           # Synonyme
                tokens = _word_in_seq()
                if tokens:
                    print(p.find_synonyms(tokens[0][0], lang_code=tokens[0][0][1]))

            elif inpt == "met":         # Metapher
                sent = input("Satz eingeben: ").lower()
                print(p.predict_metaphor(sent))

            elif inpt == "t":           # Training
                inpt2 = int(input("Wie oft? (Bei {} Epochen) ".format(epochs)))
                mlvs.train(epochs, max_iterations=inpt2)

            elif inpt == "e":           # Epochen wählen
                inpt2 = int(input("Wie viele Epochen? (jetztiger Wert {}): ".format(epochs)))
                if inpt2 > 0:
                    epochs = inpt2
                else:
                    print("ungültige Eingabe")

            elif inpt == "m":           # Metrik wählen
                inpt2 = input("Welche Metrik? (dot, norm, cos) ").lower()
                if inpt2 in ["dot", "norm", "cos"]:
                    metric = inpt2
                    print("Soll die Metrik maximiert werden? (z.B. bei 'dot' sinnvoll)")
                    inpt3 = input("Aktueller Wert: {}  (y/andere) ".format(maximize)).lower()
                    maximize = inpt3 == "y"

            elif inpt == "n":           # Radius der Metrik wählen
                N = int(input("Neues n wählen: "))

            elif inpt == "eval":        # Auswerten
                syn_p = input("Sollen Synonympaare getestet werden? (y/andere) ") == "y"
                syn_s = input("Sollen Synonymmengen getestet werden? (y/andere) ") == "y"
                meta = input("Sollen Metaphern getestet werden? (y/andere) ") == "y"
                evl.eval_predictor(synonyms_pair=syn_p, synonyms_set=syn_s, metaphors=meta)

            elif inpt == "h":           # Hilfe
                _print_help()


        except Exception as e:
            print("")
            logging.exception(e)
            print("-" * 115)
            print("Error. ('h' für Hilfe)")


run()