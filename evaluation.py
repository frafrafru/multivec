import multiprocessing as mp
import os, logging

TESTFILE_NOT_EXIST_ERROR = "Die Testdatei {} existiert nicht."
FILE_NEED_EXIST = "Stellen Sie sicher, dass die Datei {} existiert."

# Pfade zu den Auswertungsdaten
METAPHOR_PATH = os.path.dirname(os.path.realpath(__file__)) + "/src/eval/metaphor/"
SYNONYM_PATH = os.path.dirname(os.path.realpath(__file__)) + "/src/eval/synonym/"

vocab = None    # globales Vokabular
p = None        # globaler Predictor

def set_vocab(voc):
    """Setzt das globale Vokabular. Dies muss geschehen
    bevor evaluiert werden kann.
    
    :param list voc: Liste mit Vokabular, so wie sie
                     von lexical.MultiCorpus.get_vocab_list
                     bereitgestellt wird."""
    global vocab
    vocab = voc

def set_predictor(predictor):
    """Setzt den globalen Predictor. Dies muss geschehen
    bevor evaluiert werden kann.
    
    :param predictor predictor: Modul predictor.py wird
                                hier übergeben."""
    global p
    p = predictor

def eval_predictor(synonyms_pair=True, synonyms_set=False, metaphors=True):
    """Evaluiert einen Predictor in den angegebenen
    Kategorien. Zur Auswahl stehen Synonyme und Metaphern.#
    
    :param bool synonyms: Falls True werden Synonympaare/
                          -mengen evaluiert.
    :param bool metaphors: Falls True wird die Metapher-
                           erkennung evaluiert."""

    # Sollen Synonympaare evaluiert werden?
    if synonyms_pair:
        # Fange den Fall ab, dass die Datei nicht existiert (3x)
        try:
            print("\nEvaluation: Synonympaare\n" + "-" * 115)
            _evaluate(SYNONYM_PATH + "de_pair.syn", _eval_synonym)
        except FileNotFoundError:
            logging.warning(TESTFILE_NOT_EXIST_ERROR.format(SYNONYM_PATH + "de_pair.syn"))
            print(FILE_NEED_EXIST.format(SYNONYM_PATH + "de_pair.syn"))

    # Sollen Synonymmengen evaluiert werden?
    if synonyms_set:
        # ...
        try:
            print("\nEvaluation: Synonymmengen\n" + "-" * 115)
            _evaluate(SYNONYM_PATH + "de.syn", _eval_synonym2, precision=False,
                      recall=False, f_score=False, prog_per=50, batch_per=12)
        except FileNotFoundError:
            logging.exception(TESTFILE_NOT_EXIST_ERROR.format(SYNONYM_PATH + "de.syn"))
            print(FILE_NEED_EXIST.format(SYNONYM_PATH + "de.syn"))

    # sollen Metaphern evaluiert werden?
    if metaphors:
        # ...
        try:
            print("\nEvaluation: Metaphererkennung\n" + "-" * 115)
            _evaluate(METAPHOR_PATH + "de.metaphor", _eval_metaphor)
        except FileNotFoundError:
            logging.warning(TESTFILE_NOT_EXIST_ERROR.format(METAPHOR_PATH + "de.metaphor"))
            print(FILE_NEED_EXIST.format(METAPHOR_PATH + "de.metaphor"))




def _evaluate(path, func, accuracy=True, precision=True, recall=True,
              f_score=True, prog_per=1000, batch_per=250):
    """Evaluiert eine Trainingsdatei. Indem sie geöffnet wird, ex-
    trahiert wird und per Multiprozessaufruf ausgeführt wird."""

    all_results = []

    with open(path, "r") as f:

        # Evaluiere auf mehreren Prozessen
        with mp.Pool() as pool:
            batch = []
            for k, line in enumerate(f):

                batch.append(line)

                if len(batch) >= batch_per:
                    all_results += [res for res in pool.map(func, batch) if res is not None]
                    batch = []

                if k % prog_per == 0 and len(all_results):
                    # print(str(round(k/168086 * 100, 3)), "%")
                    _statistics(all_results, accuracy=accuracy, precision=precision,
                                recall=recall, f_score=f_score)

            # Falls die Batch nicht voll geworden ist
            if len(batch):
                all_results += [res for res in pool.map(func, batch) if res is not None]
    
    if len(all_results):
        _statistics(all_results, accuracy=accuracy, precision=precision,
                    recall=recall, f_score=f_score, flush=False)
        print()


def _statistics(results, accuracy=True, precision=True, recall=True, f_score=True, digits=3, flush=True):

    # Count Funktion für TP, TN, FP, FN
    c = lambda x: sum(1 for val in results if val == x)

    acc, prec, rec, fsc = tuple(4 * [0.0])
    stat_str = 4 * [""]

    total = c("tp") + c("tn") + c("fp") + c("fn")
    rnd = lambda x: str(round(x, digits))

    if accuracy:
        if total:
            acc = (c("tp") + c("tn")) / total
            stat_str[0] = "Accuracy: " + rnd(acc).ljust(7)
    if precision:
        if (c("tp") + c("fp")):
            prec = c("tp") / (c("tp") + c("fp"))
            stat_str[1] = "Precision: " + rnd(prec).ljust(7)
    if recall:
        if (c("tp") + c("fn")):
            rec = c("tp") / (c("tp") + c("fn"))
            stat_str[2] = "Recall: " + rnd(rec).ljust(7)
    if f_score:
        if not (prec == 0 and rec == 0):
            fsc = 2 * prec * rec / (prec + rec)
            stat_str[3] = "F-Score: " + rnd(fsc).ljust(7)

    out_str = "{}" * 4 + "\t - Insgesamt getestet: {}"
    print(out_str.format(*tuple(stat_str), total), end="\r", flush=flush)

def _eval_metaphor(line):

    if not p:
        logging.warning("Es wurde kein Predictor gesetzt.")
        return
    try:
        sent, is_metaphor = line.split(";")
        sent.strip()
        is_metaphor = is_metaphor.replace("\n", "").strip()

        pred = p.predict_metaphor(sent)

        return _eval_outcome(pred, is_metaphor)

    except Exception as e:
        print("Falsches Format in ", line, e)


def _eval_synonym(line):
    if not p:
        logging.warning("Es wurde kein Predictor gesetzt.")
        return
    try:
        token1, token2, answer = line.split(";")
        answer = answer.replace("\n", "").strip()
        token1 = token1.strip()
        token2 = token2.strip()

        if token1 in vocab and token2 in vocab:
            pred = p.synonymous(token1, token2)
            return _eval_outcome(pred, answer)

    except Exception as e:
        print("Falsches Format in ", line, e)

def _eval_synonym2(line):
    """Klasseninterne Methode zur Evaluation der Synonymmengen."""

    if not p:
        logging.warning("Es wurde kein Predictor gesetzt.")
        return
    K = 1
    try:
        token, syns = line.split(";")

        if token in vocab:
            # Tatsächliche und vorhergesagte Synonyme
            actual_syns = [sn for sn in syns.split(",") if sn in vocab]
            predicted_syns = p.find_synonyms((token, "de"))

            # Hilfsfunktionen zum Mengenschneiden
            # inters liefert None, falls der Versuch nicht zählt
            _inters = lambda x,y: set(x).intersection(set(y))
            inters = lambda x,y: len(_inters(x, y)) if not (y == [''] or y == []) else None

            # Anzahl Elemente im Schnitt
            S = inters(predicted_syns, actual_syns)

            # Mindestens K Elemente im Schnitt?
            if not S is None and S >= K:
                return "tp"     # Nur 'Platzhalter', die einizge sinnvolle Ausgabe
            else:               # wird hierbei Accuracy sein, daher reicht auch 0/1.
                return "fp"     # So folgt es dem Format der anderen.

    except Exception as e:
        print("Falsches Format in ", line, e)



def _eval_outcome(pred, answer, binary_decision=False):

    if not binary_decision:
        if pred == answer:
            if pred == "y":
                return "tp"
            else:
                return "tn"
        elif pred == "y":
            return "fp"
        elif pred == "n":
            return "fn"
    
    else:
        if pred == answer:
            return 1
        return 0