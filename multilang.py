import numpy as np
import os, logging
import codecs, json

from multiprocessing import Pipe, Process

from lexical import MultiCorpus
from utils import sampling_table


TRAIN_IF_NOT_EXIST_MSG = "Das Modell muss zunächst trainiert werden. (Dies kann jedoch einige Zeit dauern (~ 12-48 Stunden). Es ist daher empfehlenswert das trainierte Modell zu verwenden."
NO_MORE_DATA_WARNING = "Die Daten sind ausgegangen bevor, die Trainingsdurchläufe beendet waren."

MODEL_SAVE_PATH = os.path.dirname(os.path.realpath(__file__)) + "/save/"

class MultiLangVectorspace:

    def __init__(self, paths=None, lang_codes=None, vocab_lists=None,
                 vocab_sizes=None, translation_dict_paths=None,
                 lower=True, window_size=8, embed_dim=200):
        """Initialisiert den multilingualen Vektorraum.
        
        :param list paths: Liste von Strings die Pfade von Korpora sind.
        :param list lang_codes: Liste von Strings die Sprachcodes der
                                benutzten Sprachen enthalten, bspw. 'de',
                                'ru', etc.
        :param list vocab_lists: Liste mit Elementen, die entweder Listen
                                 von Strings (des Vokabulars) sind oder die
                                 None sind, falls das gesamte Vokabular ge-
                                 nommen werden soll (sofern vocab_sizes=None).
        :param list vocab_sizes: Liste mit Elementen, die entweder None oder
                                 integerwertig sind. Falls ungleich None, so
                                 wird das Vok. auf die häufigsten vocab_sizes
                                 Token reduziert.
        :param list translation_dict_paths: Liste von Strings, die Pfade zu
                                            Übersetzungswörterbüchern enthält.
                                            Diese Liste muss genau ein Element
                                            weniger enthalten, als die obigen.
        :param bool lower: Falls True, wird jedes Wort zunächst kleingeschrieben,
                           bevor irgendwelche Operationen durchgeführt werden."""

        # paths, lang_codes, vocabs, vocab_sizes n Elemente, bei n Sprachen
        # translation_dict_paths nur (n-1) Elemente

        self._model, self.embeds = None, None
        self.window_size = window_size
        self.embed_dim = embed_dim
    
        if not len(paths) == len(lang_codes):
            logging.exception("Die Argumente müssen korrespondieren. Stellen Sie sicher, dass paths und lang_codes dieselbe Länge haben")
            raise TypeError
            
        # Standardwerte der Parameter setzen
        if not vocab_lists:
            vocab_lists = [None] * len(paths)
        if not vocab_sizes:
            vocab_sizes = [0] * len(paths)
        if not translation_dict_paths:
            translation_dict_paths = [None] * len(paths)

        # zuerst links None anhängen, da dies für die Referenzsprache steht
        if len(translation_dict_paths) < len(paths):
            translation_dict_paths = [None] + translation_dict_paths
        while len(translation_dict_paths) < len(paths):
            translation_dict_paths = translation_dict_paths + [None]

        self.mc = MultiCorpus()       # handlet vokabular und corpus (-iteration)

        # Zunächst erste Sprache mit min. Tokengröße von 3
        self.mc.add_corpus(paths[0], lang_codes[0], vocab_lists[0],
                           vocab_sizes[0], translation_dict_paths[0],
                           lower=lower, min_token_size=3)
        # Anschließend die anderen mit min. Tokengröße von 2
        self.mc.add_corpora(paths[1:], lang_codes[1:], vocab_lists[1:],
                            vocab_sizes[1:], translation_dict_paths[1:],
                            lower=lower)
        # (min_token_size wird hier bewusst fürs Deutsche,
        # Russische und Englische gewählt.)

        # Erstelle Korpusiteratoren für die
        # Generierung der Trainingsdaten
        self.iterators = []
        for lang in lang_codes:
            self.iterators.append(self.mc.get_iterator(lang))
        # self.iterators = [self.mc.get_iterator(lang) for lang in lang_codes]

        # initialisiere das NN, übergebe Einbettungsdim.
        self._init_model(embed_dim)

        # Versuche Gewichte zu laden, falls dies nicht geht
        # möchten wir kein untrainiertes Modell liefern
        self.load(train_if_not_exist=True)

        # Erstelle ein Objekt mit den eingeb. Vektoren
        self.embeds = EmbeddedVectors(self._embed_layer, self.mc.get_mapping())


    
    def load(self, train_if_not_exist=True):
        """Versucht ein gespeichertes Modell zu laden, falls
        dies nicht funktioniert, wird per default das Training
        gestartet.

        :param bool train_if_not_exist: Falls True, wird das
                                        Training automatisch
                                        bei Nichtexistenz eines
                                        gespeicherten Modells
                                        gestartet."""

        try:
            with open(MODEL_SAVE_PATH + "model.h5", "r"):
                pass
            self._model.load_weights(MODEL_SAVE_PATH + "model.h5")

            inp = input("Es wurde ein trainiertes Modell gefunden. Soll es trotzdem trainiert werden? (y/andere)")
            if not inp == "y":
                return
        except FileNotFoundError:   # kein gespeichertes Modell gefunden
            if not train_if_not_exist:
                return
            print(TRAIN_IF_NOT_EXIST_MSG)

        try:
            it = int(input("Wie viele Durchläufe? "))
            self.train(max_iterations=it)
        except TypeError:
            print("Eine Zahl musste eingegeben werden.")


    def _init_model(self, embed_dim):

        print("Lade Keras Bibliotheken")
        from tensorflow import keras

        # erstelle zufällig Gewichte für die Tokens der Hauptsprache
        # + 1, da 0 als Paddingelement zum Input gezählt werden muss.
        num_weights = self.mc.vocab_size(self.mc.first_lang) + 1
        init_weights = np.random.rand(num_weights, embed_dim)
        
        # Skalieren: Verteilung vorher [0,1] nun auf [-0.1, 0.1]
        scale = lambda x: 0.2 * (x - 0.5)
        init_weights = scale(init_weights)

        # Kopiere nun die Gewichte der Hauptsprache in alle
        for lang in self.mc.get_languages()[1:]:
            for token in self.mc.get_vocab_list(lang):
                tok_id = -1
                try:
                    # wähle zufällig eine der Übersetzungen und deren ID aus
                    back_translated_token = np.random.choice(self.mc.translate(token, lang, self.mc.first_lang))
                    tok_id = self.mc.token_to_id(back_translated_token, self.mc.first_lang)

                # falls bei der Rückübersetzung Fehler auftreten werden 
                # diese geloggt.
                except Exception as e:
                    logging.exception(e, token, lang, self.mc.translate(token, lang, self.mc.first_lang))

                if tok_id == -1:    # keine Übersetzung gefunden
                    init_weights = np.append(init_weights, np.random.rand(1, embed_dim), axis=0)
                else:               # Wähle dasselbe Gewicht, wie von der Übersetzung
                    init_weights = np.append(init_weights, [init_weights[tok_id]], axis=0)

        # --- Erstellung der Ebenen (layers) ---
        # Einbettungsebene
        embed_layer = keras.layers.Embedding(self.mc.vocab_size() + 1, embed_dim, input_length=2,
                                             name='embed_layer', mask_zero=True)

        # Eingabeebenen
        input_target = keras.layers.Input((1,), name="input_target")
        input_context = keras.layers.Input((1,), name="input_context")

        # Ausgabeebenen der Einbettungsebene
        embed_target_out = embed_layer(input_target)
        embed_context_out = embed_layer(input_context)

        # Setze die zuvor erstellen Gewichte
        embed_layer.set_weights([init_weights])

        # Skalarprodukt + Sigmoidfunktion
        dot_product = keras.layers.dot([embed_target_out, embed_context_out], axes=(2,2))
        dot_product = keras.layers.Reshape((1,), input_shape=(1,1))(dot_product)
        output = keras.layers.Activation('sigmoid')(dot_product)

        # Initialisierung des Modells
        opt = keras.optimizers.SGD(lr=0.08, momentum=0.08)
        model = keras.Model([input_target, input_context], output)
        model.compile(loss='mse', optimizer=opt)

        # Setze das Modell klassenintern
        self._model = model
        self._embed_layer = embed_layer


    def _start_generating(self):
        """Interne Methode, um Prozess zur Datengenerierung zu
        starten.
        
        :return: Ausgang der Pipe, die die generierten Daten
                 erhält.
        :rtype: multiprocessing.Pipe"""

        # Erzeuge Pipe für den Datenfluss
        pipe_out, pipe_in = Pipe()
        proc = Process(target=self._continuously_generate, args=(pipe_in,))

        # damit dieser bei Beendigung des Programmes beendet wird
        proc.daemon = True
        proc.start()

        return pipe_out

    def _continuously_generate(self, pipe_in):
        """Interne Methode für den Prozessaufruf zur dauerhaften
        Generierung der Trainingsdaten.
        
        :param multiprocessing.Pipe pipe_in: Die Pipe, in die
                                             Trainingsdaten gesen-
                                             det werden sollen."""

        languages = self.mc.get_languages()
        i = 0

        # Erstelle die Sampling Tables
        sampling_tables = sampling_table(self.mc.get_token_counter(), self.mc.get_mapping())

        while True:
            index = i % len(self.iterators)
            sents = next(self.iterators[index])

            batch = self._generate(sents, languages[i % len(languages)], sample_tables=sampling_tables)

            # Blockiert, wenn die pipe nicht schnell genug geleert wird
            if list(map(len, batch[0]))[0] and list(map(len, batch[0]))[1] and list(map(len, batch[1]))[0]:
                pipe_in.send(batch)
                i += 1      # außerhalb des blockes (einrückung weg), falls egal ist wenn sprachen übersprungen werden, da sie gerade keinen passenden satz zur verfügung stellen können


    def _generate(self, sents, lang_code, negative_sampling=True,
                  sample_tables=None, num_negative_samples=20,
                  languages=None):
        """Interne Methode, um die Trainingsdaten zu generieren.
        Hier ist das Herzstück der Trainingslogik.
        
        :param list sents: Liste mit Sätzen
        :param str lang_code: Sprachchode, wie z.B. 'de', welcher
                              zum Korpus gehört.
        :param bool negative_sampling: Falls True, wird die sog.
                                       noise contrastive estimation
                                       durchgeführt.
        :param tuple sample_tables: Tupel, die zwei Liste zu
                                    Sampling Tables enthält.
                                    Diese können abgerufen werden
                                    via utils.sampling_table.
        :param int num_negative_samples: Anzahl der Tokens, die
                                         in der noise contrastive
                                         estimation ausgewählt wer-
                                         den.
        :param list languages: Liste von Sprachcodes, dessen Über-
                               setzungen mit aktiviert werden sollen.
                               
        :return: Trainingsdatensatz der Form X, y, die an das Modell,
                 welches über MultiLangVectorspace._init_model gesetzt
                 werden kann.
        :rtype: tuple"""

        if not languages:
            languages = self.mc.get_languages()

        keep_probabilities, neg_probabilities = sample_tables
        pad = lambda x, y: ([''] * y) + x + ([''] * y)

        # Grundraum für negatives Sampling
        Omega = np.array([j for j in range(self.mc.vocab_size())])
        Omega += 1      # Indizes fangen ab 1 an

        # Initialisiere Batch
        X, y = [np.array([])] * 2, [np.array([])]

        for sent in sents:
            # Leeren Kontext initialisieren
            context = [(0, '', 0)] * (2 * self.window_size + 1)
            
            # Über jedes Token im gepaddeten Satz iterieren
            for token in pad(sent, self.window_size):

                # Halte konstante Kontextgröße
                if len(context) >= (2 * self.window_size + 1):
                    context.pop(0)

                # Füge zum Kontext hinzu, falls im Vokabular
                if self.mc.in_vocab(token, lang_code):
                    glob_id = self.mc.token_to_id(token, lang_code)
                    lang_id = self.mc.token_to_id(token, lang_code, glob=False)
                    context.append((glob_id, token, lang_id))
                else:
                    context.append((0, '', 0))


                # --- AKTIVIERUNG DES ZIELTOKENS ---
                # Der Übersicht halber setze target_token
                target_token = context[self.window_size]
                # Ueberpruefe, ob Zieltoken nicht OOV ist
                if target_token[0]:

                    # Bernoulliexperiment mit WSkeit p_keep
                    p_keep = keep_probabilities[target_token[2]]
                    if np.random.choice([1, 0], p=[p_keep, 1 - p_keep]):

                        # Iteriere ueber den Kontext
                        for cotoken_id, _, cotok_lang_id in context:

                            # Falls cotok im Vokabular und nicht das Zieltoken 
                            if cotoken_id and not cotoken_id == target_token[0]:
                                p_keep = keep_probabilities[cotok_lang_id]
                                P = [p_keep, 1 - p_keep]

                                # Bernoulliexperiment mit WSkeit p_keep
                                if np.random.choice([1, 0], p=P):

                                    # Trainingsdaten zur Batch hinzufuegen
                                    X[0] = np.append(X[0], [target_token[0]])
                                    X[1] = np.append(X[1], [cotoken_id])
                                    y[0] = np.append(y[0], [1])

                # --- AKTIVIERUNG DER ÜBERSETZUNGEN ---
                # Iteration über alle Sprachen
                for lang in languages:
                    if lang == lang_code:
                        continue    # keine Übersetzung in dieselbe Sprache

                    # Erhalte Übersetzungen
                    translate = self.mc.translate(target_token[1], lang_code, lang)

                    # den richtigen Index in der Zielsprache waehlen
                    t_index = lambda x: self.mc.token_to_id(x, lang)
                    t_indices = [t_index(trans) for trans in translate]

                    # Setze Übersetzung zu Token in Verbindung
                    for trans_index in t_indices:

                        # (Token, Übersetzung) hat Ausgabe 1
                        X[0] = np.append(X[0], [target_token[0]])
                        X[1] = np.append(X[1], [trans_index])
                        y[0] = np.append(y[0], [1])


                # --- AKTIVIERUNG DES RAUSCHENS ---
                # noise contrastive estimation
                if negative_sampling:
                    sample = np.random.choice(Omega, num_negative_samples, p=neg_probabilities, replace=False)

                    # Füge jedes negative Sample zum Trainingsdatensatz hinzu
                    X[0] = np.append(X[0], [target_token[0]] * num_negative_samples)
                    X[1] = np.append(X[1], [nontoken_id for nontoken_id in sample])
                    y[0] = np.append(y[0], [0.5] * num_negative_samples)

        return X, y


    def train(self, epochs=12, max_iterations=None, save_after=5, show_progress=10):

        # Für die Fortschrittanzeige
        if not show_progress == 0:
            progress_str = "Progress: DE_ITER: {}, RU_ITER: {}, EN_ITER: {}"
            prog = lambda x: tuple([it.progress() for it in x])

        counter = 0

        # Erhalte eine Pipe, die Batches zu Verfuegung stellt
        pipe_out = self._start_generating()
        print("Training gestartet.")

        while True:
            if pipe_out.poll():
                try:
                    X, y = pipe_out.recv()
                    self._model.fit(X, y, epochs=epochs)
                    counter += 1

                    # Bei jedem save_after-ten Durchlauf wird gespeichert
                    if counter % save_after == 0:
                        print("Automatisches Speichern...")
                        self._model.save_weights(MODEL_SAVE_PATH + "model.h5")

                    # Falls Fortschritt ausgegeben werden soll
                    if not show_progress == 0:
                        if counter % show_progress == 0:
                            print(progress_str.format(prog(self.iterators)))

                except EOFError:
                    logging.warning(NO_MORE_DATA_WARNING)
                    break
            
            # Ueberpruefe ob maximale Anzahl an Iterationen erreicht
            if max_iterations and counter >= max_iterations:
                break

        # Falls nicht schon geschehen, speichere das Modell
        if not counter % save_after == 0:
            self._model.save_weights(MODEL_SAVE_PATH + "model.h5")

        print("Training beendet!")


    def most_similar(self, positive, negative=[], num=15, metric="norm",
                     maximize=False, lang_code=None):
        """Bei Eingabe einer Liste von Tokens bzw. ein Token
        via positive und optional gleiches für negative wird
        die Summe der Vektoren aller Tokens bestimmt, wobei
        die Vektoren der negativen zunächst mit -1 multipliziert
        werden. Gibt die nächsten Vektoren um den Ursprung herum zurück,
        falls das Token nicht im Vokabular ist.
        
        :param list positive: Liste von Tokens oder Token
                              im Format [('token', 'de'), ..]
                              bzw. ('token', 'de'), etc. wessen
                              Vektoren positiv summiert werden.
        :param lsit negative: Liste von Tokens oder Token
                              im Format [('token', 'de'), ..]
                              bzw. ('token', 'de'), etc. wessen
                              Vektoren negativ summiert werden.
        :param int num: Anzahl von Tokens, die zurückgegeben
                        werden.
        :param str metric: Spezifiziert die Metrik. Mögliche Wahlen:
                            - 'norm' (eukl. Norm),
                            - 'dot' (eukl. Skalarprodukt)
        :param bool maximize: Soll die Metrik maximiert werden?
                              Sonst minimiert.
        :param str lang_code: Falls angegeben, werden nur Tokens
                              dieser Sprache zurückgegeben.
        
        :return: Nach Metrik sortierte (je nach Metrik auf- bzw.
                 absteigend) Liste der Tokens
        :rtype: list, Format [(Wert d.Metrik, ('token', 'de')), ...]"""

        # Initialisiere Nullvektor
        token_vector = np.array([0.0 for _ in range(self.embed_dim)])

        # Überprüfe ob Liste oder Token
        if isinstance(positive, list):
            for token, lang in positive:
                token_vector += np.array(self.embeds.get_vector(token, lang))
        else:
            token_vector += np.array(self.embeds.get_vector(positive[0], positive[1]))

        # Überprüfe ob Liste oder Token
        if isinstance(negative, list):
            for token, lang in negative:
                token_vector -= np.array(self.embeds.get_vector(token, lang))
        else:
            token_vector -= np.array(self.embeds.get_vector(negative[0], negative[1]))

        # Erhalte den nächsten Vektor zu dem oben bestimmten
        return self.embeds.get_closest(token_vector, num=num, metric=metric,
                                       maximize=maximize, lang_code=lang_code)

    def similarity(self, token1, token2, metric="norm"):
        """Gibt den zur Metrik gehörenden Ähnlichkeits-
        wert der beiden angegebenen Tokens zurück.
        
        :param tuple token1: Token im Format ('token1', 'de'), etc.
        :param tuple token2: Token im Format ('token2', 'de'), etc.
        :param str metric: mögliche Metriken, die gewählt werden können:
                           norm, dot, cos (eukl. Norm, St.Skalarprodukt, 
                           Kosinus-Ähnlichkeit)

        :return: Ähnlichkeitswert der beiden Tokens
        :rtype: int"""

        vec1 = self.embeds.get_vector(token1[0], token1[1])
        vec2 = self.embeds.get_vector(token2[0], token2[1])

        return self.embeds.sim(vec1, vec2, metric=metric)


class EmbeddedVectors:
    """Speichert ein gelerntes Word-Embedding aus der Klasse
    MultiLangVectorspace"""

    def __init__(self, embed_layer, mapping):
        """Initialisiert ein Word-Embedding.
        
        :param keras.layers.Embedding embed_layer: Einbettungs-
                                                   ebene, des
                                                   gelernten
                                                   Keras-Modells.
        :param dict mapping: Indexzuordnung im dict Format. Token
                             auf Index."""

        vectors = embed_layer.get_weights()[0]
        self.mapping = mapping
        self.id_to_vector = {token_id: np.array(vectors[token_id]) for _, token_id in mapping.items()}
        self.token_to_vector = {token: np.array(self.id_to_vector[token_id]) for token, token_id in mapping.items()}


    def get_vector(self, token, lang):
        """Gibt die Vektorrepräsentation des Tokens
        token der Sprache lang zurück.
        
        :param str token: Token, dessen Vektorrepräsentation
                          zurückgegeben werden soll.
        :param str lang: Sprachcode der zugehörigen Sprache.
        
        :return: Vektorrepräsentation des Tokens token
        :rtype: numpy.array"""

        try:
            return self.token_to_vector[(token, lang)]
        except KeyError:
            logging.info("Token ({}, {}) nicht gefunden.".format(token, lang))
            return np.array([0.0] * len(list(self.id_to_vector.items())[0][1]))     # Nullvektor

    def id_to_vec(self, token_id, lang):
        """Gibt den Vektor der zugehörigen Token ID
        (Index) inder Sprache lang zurück.
        
        :param int token_id: Token ID (Index) des
                             gewünschten Vektors.
        :param str lang: Sprachcode der zug. Sprache.
        
        :return: Vektorrepräsentation des zu token_id
                 gehörigen Tokens.
        :rtype: numpy.array"""

        return self.id_to_vector[(token_id, lang)]

    def get_vectors(self):
        """Gibt alle Vektorrepräsentationen als
        Wörterbuch (dict) mit Tokens als Keys zurück.
        
        :return: Token-auf-Vektor-Abbildungswörterbuch
        :rtype: dict"""
        return self.token_to_vector

    def get_noise_words(self, metric="norm"):
        """Gibt alle Wörter zurück, die häufig
        neben anderen auftreten. (Wird derzeit nicht verwendet)
        
        :param str metric: Metrik die verwendet werden
                           soll.
        
        :return: sortiertes Wörterbuch nach durchschnittl.
                 Auftretenshäufigkeit in der Nähe aller
                 anderen Tokens.
        :rtype: dict"""

        from collections import Counter
        c = Counter()
        dists = dict()

        for token, vec in self.get_vectors().items():

            sims = self.get_closest(vec, num=15, metric=metric)
            for dist, sim in sims:
                c[sim] += 1
                dists.setdefault(token, []).append(dist)

        avg_dists = {token: np.mean(value) for token, value in dists.items()}
        avg_dists = sorted(avg_dists, key=lambda x:x[1], reverse=(metric=="dot"))[:60]
        return avg_dists


    def get_closest(self, vec, num=20, metric="norm",maximize=False,
                    lang_code=None):
        """Bestimmt die nächsten Vektoren bezüglich der
        angegebenen Metrik.

        :param numpy.array vec: Ausgangsvektor
        :param int num: Anzahl der nächsten Vektoren, die bestimmt
                        werden.
        :param str metric: mögliche Metriken, die gewählt werden können:
                           norm, dot, cos (eukl. Norm, St.Skalarprodukt, 
                           Kosinus-Ähnlichkeit)
        :param bool maximze: Gibt an, ob nach dem größten bzw. kleinsten
                             Wert (in der angegebenen Metrik) sortiert
                             werden soll.
        :param str lang_code: Sprachcode angeben, falls nur Vektoren
                              dieser Sprache berücksichtig werden sollen.

        :return: Die nächsten Vektoren zum angegebenen. Im Format:
                 [(Wert d. Metrik, Token), ...]
        :rtype: list"""

        # wähle Metrik
        metric_func = lambda x: self._sim(metric=metric)(x, vec)

        # berechne Abstände aller anderen Vektoren und sortiere
        dists = [(metric_func(target_vec), target_token)
                 for target_token, target_vec in self.get_vectors().items()]
        dists = sorted(dists, key=lambda x: x[0], reverse=maximize)

        # wenn es egal ist, zu welcher Sprache diese gehören
        if lang_code is None:
            return dists[:num]

        # andernfalls werden nur die Tokens der
        # jeweiligen Sprache zurückgegeben
        else:
            new_dists = []
            for dist, (token, lang) in dists:
                if lang == lang_code:
                    new_dists.append((dist, token))
            return new_dists[:num]

    def sim(self, vec1, vec2, metric="norm"):
        """Gibt die Ähnlichkeit zweier Vektoren
        bezüglich der angegeben Metrik zurück.
        
        :param numpy.array vec1: erster Vektor
        :param numpy.array vec2: zweiter Vektor
        :param str metric: gewählte Metrik
        
        :return: Ähnlichkeitswert der Vektoren
        :rtype: float"""

        return self._sim(metric=metric)(vec1, vec2)
    
    def _sim(self, metric):
        """Klasseninterne Methode zur Berechnung der
        Ähnlichkeit."""

        if metric == "dot":
            return lambda x, y: np.dot(x, y)
        elif metric == "norm":
            return lambda x, y: np.linalg.norm(x - y)
        elif metric == "cos":
            return lambda x, y: np.dot(x, y) / (np.linalg.norm(x) * np.linalg.norm(y))
